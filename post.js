document.addEventListener('DOMContentLoaded', function() {

    var password = ""
    var chanel = ""
    var token = ""
    chrome.storage.local.get(["chanel", 'password'], function(items){
        chanel = items.chanel
        password = items.password
        // Connect to server
        var socket = io.connect('wss://betterauthenticator-server.onrender.com:443/')
        // Tells server which room to listen to
        socket.emit('set_room', {room : chanel})

        // On message from phone
        socket.on("tocomputer", (data) => {
            // Message recieved
            message = data.message;
            // Decrypt Message
            var decrypted = CryptoJS.AES.decrypt(message, password);
            var demessage = decrypted.toString(CryptoJS.enc.Utf8)
            
            // On Sync
            if (demessage.substring(0,9) == "connected") {
                // Save sync data
                chrome.storage.local.set({ "password": password, "chanel": chanel, "computerList": JSON.parse(demessage.substring(10,)) }, function(){});

                // Toast Synced
                document.getElementById("checkdiv").style.display = "block"
                document.getElementById("checktext").innerText = "synced"
                setTimeout(function () {
                    document.getElementById("checkdiv").style.display = "none"
                }, 3000);

            // Token recieved
            } else if (demessage.substr(0,5) == "totp:") {
                // Set token and token text
                token = demessage.substr(5,)
                document.getElementById("totpnum").innerText = token.substr(0,3) + " " + token.substr(3,)
                // Change div to gottotp
                document.getElementById("notfounddiv").style.display = 'none';
                document.getElementById("gottotp").style.display = 'block';
                document.getElementById("founddiv").style.display = 'none';
            // Token and fill command recieved
            } else if (demessage.substr(0,5) == "fotp:") {
                // Set token and token text
                token = demessage.substr(5,)
                document.getElementById("totpnum").innerText = token.substr(0,3) + " " + token.substr(3,)
                // Change div to gottotp
                document.getElementById("notfounddiv").style.display = 'none';
                document.getElementById("gottotp").style.display = 'block';
                document.getElementById("founddiv").style.display = 'none';
                // Set OTP input to totp token
                chrome.scripting.executeScript({
                    // code: 'var inputs = document.getElementsByTagName("input");for (var i = 0; i < inputs.length; ++i) {inputs[i].value = "' + token + '";}'
                    target: {tabId: tab.id},
                    func: autofill,
                    args: [token]
                });
                // Toast Filled
                document.getElementById("checkdiv").style.display = "block"
                document.getElementById("checktext").innerText = "filled"
                setTimeout(function () {
                    document.getElementById("checkdiv").style.display = "none"
                }, 1000);
            }
        });
    });


    // Set global variable url (and current tab)
    var url = ""
    var tab;
    chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
        url = tabs[0].url;
        tab = tabs[0];
        url = url.replace('http://','').replace('https://','').split(/[/?#]/)[0]
        document.getElementById("logo").src = tabs[0].favIconUrl
    });



    
    // Log Out Button Handler
    var logoutButton = document.getElementById('logout');
    logoutButton.addEventListener('click', function() {
        chrome.storage.local.set({ "password": "", "chanel": "" }, function(){});

        window.location.href = '/pre.html';
    }, false);



    // Check storage against url
    chrome.storage.local.get(["computerList"], function(items){
        try {
            let list = items["computerList"];
            var foundname = ""
            // Loop through accounts
            for (var key in list) {
                // Test account name and sites against url
                if ((list[key].site != "" & list[key].site.toLowerCase().includes(url)) || url.includes(list[key].issuer.toLowerCase())) {
                    // Only add the first match found
                    if (foundname == "") {
                        foundname = list[key].issuer
                    }
                }
                // Add list of *all* sites to the associate dropdown
                // and the search dropdown
                associate_select = document.getElementById("associate")
                search_select = document.getElementById("search_select")
                var opt = document.createElement('option');
                var searchk = document.createElement('option');
                opt.value = list[key].issuer;
                searchk.value = list[key].issuer;
                opt.innerText = list[key].issuer;
                associate_select.appendChild(opt);
                search_select.appendChild(searchk);
            }
            // Set name and logo of founddiv
            document.getElementById("name").innerText =  foundname.toLowerCase()
            var logo = document.getElementById("logo")
            // Just hope that the favicon exists
            logo.src = "http://" + url + "/favicon.ico"
            // If image doesnt exist, default to icon.png
            if (!logo.complete) {
                logo.onerror = () => {
                    logo.src = "assets/icon.png"
                };
            }
        } catch (e) {
            console.log("THERES AN ERROR. ID: PO1");
            console.log(e)
        }
        // If no match found
        if (foundname == "") {
            // Set div to notfounddiv
            document.getElementById("founddiv").style.display = 'none';
            document.getElementById("gottotp").style.display = 'none';
            document.getElementById("notfounddiv").style.display = 'block';
            var temp = "No result found for: " + url + "."
            document.getElementById("notfoundtext").innerText = temp;
            document.getElementById("notfoundassociate").innerText = "Already have this key?\nAssociate '" + url + "' with an existing key:";

        // If a match was found
        } else {
            // Set div to founddiv
            document.getElementById("notfounddiv").style.display = 'none';
            document.getElementById("gottotp").style.display = 'none';
            document.getElementById("founddiv").style.display = 'block';
        }
    });

    // Request Button Listener
    var requestButton = document.getElementById('request');
    requestButton.addEventListener('click', function() {
        // Request Button was pressed

        // Extract domain from url e.g. lastpass from www.lastpass.com
        var domains = [".com", ".org", ".net", ".co", ".int", ".mil", ".edu", ".gov", ".biz", ".info", ".jobs", ".mobi", ".name", ".ly", ".tel", ".kitchen", ".email", ".tech", ".estate", ".xyz", ".codes"," .bargains", ".bid", ".expert",  ".us" ]
        for (let i = 0; i < domains.length; i++) {
            if (url.indexOf(domains[i]) != -1) {
                name = url.substr(0,url.indexOf(domains[i]))
                name = name.substr(name.lastIndexOf(".")+1,)
                break
            }
        }

        // Encrypt name and setup message text
        let message = "Request:"+CryptoJS.AES.encrypt(name, password).toString()

        // Send the message
        var socket = io.connect('wss://betterauthenticator-server.onrender.com:443/')
        socket.emit('set_room', {room : chanel})
        socket.emit('send_notification', {message: "Click here to send the data", title: "You have a key request", keyrequest: message})
    }, false);


    // Search Input Enter Listener
    const element = document.getElementById('search');
    element.addEventListener('submit', event => {
        // Don't refresh on press
        event.preventDefault();

        //Gotta encrypt the name of the website, dont want anyone to know you are requesting a specific bank or other.
        let message = "Request:"+CryptoJS.AES.encrypt(document.getElementById("searchinput").value, password).toString();

        // Send the request
        var socket = io.connect('wss://betterauthenticator-server.onrender.com:443/')
        socket.emit('set_room', {room : chanel})
        socket.emit('send_notification', {message: "Click here to send the data", title: "You have a key request", keyrequest: message})
    });

    // Search Button Press Listener
    var search_submit = document.getElementById('search_submit');
    search_submit.addEventListener('click', function() {
        //Gotta encrypt the name of the website, dont want anyone to know you are requesting a specific bank or other.
        let message = "Request:"+CryptoJS.AES.encrypt(document.getElementById("searchinput").value, password).toString();
        
        // Send the request
        var socket = io.connect('wss://betterauthenticator-server.onrender.com:443/')
        socket.emit('set_room', {room : chanel})
        socket.emit('send_notification', {message: "Click here to send the data", title: "You have a key request", keyrequest: message})
    }, false);


    // Associate Button Press Listener
    var associate_submit = document.getElementById('associate_submit');
    associate_submit.addEventListener('click', function() {
        //Gotta encrypt the name of the website, dont want anyone to know you are requesting a specific bank or other.
        let message = "Assoc:"+CryptoJS.AES.encrypt(document.getElementById("associate").value+":"+url, password).toString();

        // Send the request
        var socket = io.connect('wss://betterauthenticator-server.onrender.com:443/')
        socket.emit('set_room', {room : chanel})
        socket.emit('send_notification', {message: "Click here to send the data", title: "You have a key request", keyrequest: message})

        // Save the association
        chrome.storage.local.get(["computerList"], function(items){
            items = items["computerList"]
            // Loop all accounts and add association if request is same name
            for (var key in items) {
                // Note, if ther person has more than 1 of the same issuer, its fine to relate all of them to the site
                if (document.getElementById("associate").value == items[key].issuer) {
                    items[key].site += ", " + url
                }
            }
            // Save the data
            chrome.storage.local.set({ "computerList": items}, function(){});
        })
    }, false);


    // Copy Button Press Listener
    var copyButton = document.getElementById('copybutton');
    copyButton.addEventListener('click', function() {
        // Copy text to clipboard
        navigator.clipboard.writeText(token)
        // Toast Copy
        document.getElementById("checkdiv").style.display = "block"
        document.getElementById("checktext").innerText = "copied"
        setTimeout(function () {
            document.getElementById("checkdiv").style.display = "none"
        }, 1000);
    }, false);

    // Make input selected on open
    //document.getElementById("searchinput").focus()

    // Lastpass/Extras
    // Empty Extra Press Listener
    var extra_1 = document.getElementById("extra_1");
    extra_1.addEventListener('click', function() {
		window.location.href = '/extra_setup.html';
    })
    var extra_2 = document.getElementById("extra_2");
    extra_2.addEventListener('click', function() {
		window.location.href = '/extra_setup.html';
    })

    // Extra 1 delete Button Listener
    var extra_x_1 = document.getElementById("extra_x_1");
    extra_x_1.addEventListener('click', function() {
        // Remove the first extra from storage
        chrome.storage.local.get(["extras"], function(items){
            items = items["extras"]
            items.shift();
            chrome.storage.local.set({ "extras": items}, function(){})
            window.location.href = '/post.html';
        })

    })
    // Extra 2 delete Button Listener
    var extra_x_2 = document.getElementById("extra_x_2");
    extra_x_2.addEventListener('click', function() {
        // Remove the second extra from storage
        chrome.storage.local.get(["extras"], function(items){
            items = items["extras"]
            items.pop();
            chrome.storage.local.set({ "extras": items}, function(){})
            window.location.href = '/post.html';
        })
    })

    // Extra 1 With Content Press Listener
    var extra_1_with = document.getElementById("extra_1_with");
    extra_1_with.addEventListener('click', function() {
        // Request the extra 1 key
        chrome.storage.local.get(["extras"], function(items){
            items = items["extras"]
            request = items[0][2]
            //Gotta encrypt the name of the website, dont want anyone to know you are requesting a specific bank or other.
            let message = "Request:"+CryptoJS.AES.encrypt(request, password).toString();
            var socket = io.connect('wss://betterauthenticator-server.onrender.com:443/')
            socket.emit('set_room', {room : chanel})
            socket.emit('send_notification', {message: "Click here to send the data", title: "You have a key request", keyrequest: message})
        })
    })
    // Extra 2 With Content Press Listener
    var extra_2_with = document.getElementById("extra_2_with");
    extra_2_with.addEventListener('click', function() {
        // Request the extra 2 key
        chrome.storage.local.get(["extras"], function(items){
            items = items["extras"]
            request = items[1][2]
            //Gotta encrypt the name of the website, dont want anyone to know you are requesting a specific bank or other.
            let message = "ExtraRequest:"+CryptoJS.AES.encrypt(request, password).toString();
            var socket = io.connect('wss://betterauthenticator-server.onrender.com:443/')
            socket.emit('set_room', {room : chanel})
            socket.emit('send_notification', {message: "Click here to send the data", title: "You have a key request", keyrequest: message})
        })
    })


    // Set up extras
    chrome.storage.local.get(["extras"], function(items){
        items = items["extras"]
        // Set the first extra if it exists
        if (items.length > 0) {
            item = items[0]
            document.getElementById("extra_1").style.display = "none";
            document.getElementById("extra_1_with").style.display = "block";

            document.getElementById("extra_1_with_text").innerText = item[1];
            document.getElementById("extra_1_with").style.backgroundColor = item[0];
        }
        // Set the second extra if it exists
        if (items.length == 2) {
            item = items[1]
            document.getElementById("extra_2").style.display = "none";
            document.getElementById("extra_2_with").style.display = "block";

            document.getElementById("extra_2_with_text").innerText = item[1];
            document.getElementById("extra_2_with").style.backgroundColor = item[0];
        }

    })

    function test() {
        alert("testing");
        betterauthenticator_totp = "654456"
    }

    // Fill Button Press Listener
    var inputButton = document.getElementById('inputbutton');
    inputButton.addEventListener('click', function() {

        // Set OTP input to totp token
        chrome.scripting.executeScript({
            // code: 'var inputs = document.getElementsByTagName("input");for (var i = 0; i < inputs.length; ++i) {inputs[i].value = "' + token + '";}'
            target: {tabId: tab.id},
            func: autofill,
            args: [token]
        });
        // Toast Filled
        document.getElementById("checkdiv").style.display = "block"
        document.getElementById("checktext").innerText = "filled"
        setTimeout(function () {
            document.getElementById("checkdiv").style.display = "none"
        }, 1000);
    }, false);

}, false);
